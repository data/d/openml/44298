# OpenML dataset: Meta_Album_DOG_Mini

https://www.openml.org/d/44298

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Dogs Dataset (Mini)**
***
Researchers from Stanford University created the original Dogs dataset (http://vision.stanford.edu/aditya86/ImageNetDogs/). It contains more than 20 000 images belonging to 120 breeds of dogs worldwide. The images and annotations came from ImageNet for the task of fine-grained image categorization. The number of images per class and the resolution is not balanced. Each class can have 148 to 252 images with a resolution from 100x105 to 2 448x3 264 px. This dataset has a little inter-class variation and a large intra-class variation due to color, pose, and occlusion. Most of the images in this dataset are taken in man-made environments leading to a significant background variation. The preprocessed version of this dataset is prepared from the original dataset by cropping the images from either side to make squared images. In case an image has a resolution lower than 128 px, the squared images are done by either duplicating the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image. These square images are then resized into 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/DOG.png)

**Meta Album ID**: LR_AM.DOG  
**Meta Album URL**: [https://meta-album.github.io/datasets/DOG.html](https://meta-album.github.io/datasets/DOG.html)  
**Domain ID**: LR_AM  
**Domain Name**: Large Aninamls  
**Dataset ID**: DOG  
**Dataset Name**: Dogs  
**Short Description**: Dogs dataset with different breeds of dogs  
**\# Classes**: 120  
**\# Images**: 4800  
**Keywords**: dogs, animals  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Cite to use dataset, open for research  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: Stanford Dogs Dataset  
**Source URL**: http://vision.stanford.edu/aditya86/ImageNetDogs/  
  
**Original Author**: Aditya Khosla, Nityananda Jayadevaprakash, Bangpeng Yao, Li Fei-Fei  
**Original contact**: aditya86@cs.stanford.edu  

**Meta Album author**: Dustin Carrion  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{KhoslaYaoJayadevaprakashFeiFei_FGVC2011,
    author = {Aditya Khosla and Nityananda Jayadevaprakash and Bangpeng Yao and Li Fei-Fei},
    title = {Novel Dataset for Fine-Grained Image Categorization},
    booktitle = {First Workshop on Fine-Grained Visual Categorization, IEEE Conference on Computer Vision and Pattern Recognition},
    year = {2011},
    month = {June},
    address = {Colorado Springs, CO}
}


```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44313)  [[Extended]](https://www.openml.org/d/44331)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44298) of an [OpenML dataset](https://www.openml.org/d/44298). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44298/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44298/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44298/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

